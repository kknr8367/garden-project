export const environment = {
  production: true,
  isEnableMock: true,
  appName: 'GENERAL MOTORS',
  apiEndPoint: '/api/v1',
  socketUrl: 'http://localhost:8988',
};
