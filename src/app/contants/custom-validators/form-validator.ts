import {
  FormControl,
  AbstractControl,
  FormGroup,
  NgForm,
  FormGroupDirective,
} from '@angular/forms';
import { AbstractClassPart } from '@angular/compiler/src/output/output_ast';

export function passValidator(control: AbstractControl) {
  if (control && (control.value !== null || control.value !== undefined)) {
    const cnfpassValue = control.value;

    const passControl = control.root.get('password');
    if (passControl) {
      const passValue = passControl.value;
      if (passValue !== cnfpassValue || passValue === '') {
        return {
          passValidator: true,
        };
      }
    }
  }

  return null;
}

//   export class PasswordValidator {
//     // Inspired on: http://plnkr.co/edit/Zcbg2T3tOxYmhxs7vaAm?p=preview
//     static areEqual(formGroup: FormGroup) {
//       let value;
//       let valid = true;
//       for (let key in formGroup.controls) {
//         if (formGroup.controls.hasOwnProperty(key)) {
//           let control: FormControl = <FormControl>formGroup.controls[key];

//           if (value === undefined) {
//             value = control.value
//           } else {
//             if (value !== control.value) {
//               valid = false;
//               break;
//             }
//           }
//         }
//       }

//       if (valid) {
//         return null;
//       }

//       return {
//         areEqual: true
//       };
//     }
//}
