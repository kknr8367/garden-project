import { MenuElement } from '@app/models/menu-element';

/**
 * @important
 * link: must be start with forward slash and give full path of that router
 */

export const menus: MenuElement[] = [
  // {
  //   name: 'Dashboard',
  //   icon: 'dashboard',
  //   link: '/dashboard',
  //   tooltip: 'Dashboard',
  // },
  {
    name: 'Sessions',
    svgIcon: 'session',
    link: '/sessions',
    tooltip: 'Sessions',
  },
];
