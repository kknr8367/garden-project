import { environment } from '@env/environment';

const apiEndPoint = environment.apiEndPoint;

export const apiRoutes = {
  AUTH: {
    LOGIN: `${apiEndPoint}/auth/login`,
    LOGOUT: `${apiEndPoint}/logout`,
    AUTHENTICATE: `${apiEndPoint}/authenticate`,
  },
  SESSIONS: {
    LIST: `${apiEndPoint}/sessions`,
  },
};
