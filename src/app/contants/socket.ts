import { environment } from '@env/environment';
import { SocketIoConfig } from 'ngx-socket-io';

export const socketIoConfig: SocketIoConfig = {
  url: environment.socketUrl,
  options: {},
};
