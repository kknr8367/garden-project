import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { snakeCase } from 'lodash';
import { DataService } from './data.service';

@Injectable({
  providedIn: 'root',
})
export class LocalStorageService {
  userInfo: string = 'USER_INFO';
  authToken: string = 'AUTH_TOKEN';

  constructor(private dataService: DataService) {}

  private prefix: string = `${environment.appName || 'MY_APP'}_`;

  private getStorageName(name: string): string {
    return snakeCase(this.prefix + name).toUpperCase();
  }

  set(name: string, data: any): any {
    name = this.getStorageName(name);
    let stringify = JSON.stringify(data);
    return localStorage.setItem(name, stringify);
  }

  get(name: string): any {
    name = this.getStorageName(name);
    let item = localStorage.getItem(name) as any;
    return JSON.parse(item);
  }

  remove(name: string) {
    name = this.getStorageName(name);
    localStorage.removeItem(name);
  }

  clear() {
    localStorage.clear();
  }

  setAuth(data: any) {
    this.set(this.userInfo, data.user);
    this.set(this.authToken, data.token);
    this.dataService.setUserInfo(data.user);
  }

  removeAuth() {
    this.remove(this.userInfo);
    this.remove(this.authToken);
    this.dataService.setUserInfo(null);
  }
}
