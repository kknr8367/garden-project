import { Injectable } from '@angular/core';
import { UserInfo } from '@app/models/user-info';
import { BehaviorSubject } from 'rxjs';
import { LocalStorageService } from './local-storage.service';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  private _userInfo = new BehaviorSubject<UserInfo | unknown>(null);

  constructor() {}

  getUserInfo() {
    return this._userInfo.asObservable();
  }

  setUserInfo(userInfo: UserInfo | unknown) {
    this._userInfo.next(userInfo);
  }
}
