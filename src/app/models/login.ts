export interface LoginRequest {
  emailId: string;
  password: string;
}

export interface LoginResponse {
  user?: any;
  token: string;
}
