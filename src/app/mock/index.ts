import { environment } from '@env/environment';
import { AuthService, AuthMockService } from '../core/services/auth';

const providers = [
  {
    provide: AuthService,
    useClass: AuthMockService,
  },
];

export const mockServiceProviders = environment?.isEnableMock ? providers : [];
