import { LoginRequest, LoginResponse } from '@app/models/login';
import { Observable } from 'rxjs';

export interface IAuthService {
  getAuthorizationToken(): string;
  isAuthenticated(): boolean;
  authenticate(): Observable<any>;
  login(data: LoginRequest): Observable<LoginResponse>;
  logout(): Observable<any>;
}
