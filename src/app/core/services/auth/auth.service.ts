import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { apiRoutes } from '@app/contants';
import { LoginRequest, LoginResponse } from '@app/models/login';
import { LocalStorageService } from '@app/shared/services/local-storage.service';
import { Observable } from 'rxjs';
import { IAuthService } from './auth.service.interface';

@Injectable({
  providedIn: 'root',
})
export class AuthService implements IAuthService {
  constructor(
    private http: HttpClient,
    private localStorage: LocalStorageService
  ) {}

  getAuthorizationToken(): string {
    return this.localStorage.get(this.localStorage.authToken) || '';
  }

  isAuthenticated(): boolean {
    // Check to see if user is authenticated
    return !!this.getAuthorizationToken();
  }

  authenticate(): Observable<any> {
    return this.http.get<any>(apiRoutes.AUTH.AUTHENTICATE);
  }

  login(data: LoginRequest) {
    return this.http.post<LoginResponse>(apiRoutes.AUTH.LOGIN, data);
  }

  logout(): Observable<any> {
    return this.http.get(apiRoutes.AUTH.LOGOUT);
  }
}
