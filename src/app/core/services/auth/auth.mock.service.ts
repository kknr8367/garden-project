import { Injectable } from '@angular/core';
import { LoginRequest, LoginResponse } from '@app/models/login';
import { UserInfo } from '@app/models/user-info';
import { LocalStorageService } from '@app/shared/services/local-storage.service';
import { Observable, of } from 'rxjs';
import { IAuthService } from './auth.service.interface';

@Injectable({
  providedIn: 'root',
})
export class AuthService implements IAuthService {
  constructor(private localStorage: LocalStorageService) {}

  getAuthorizationToken(): string {
    return this.localStorage.get(this.localStorage.authToken) || '';
  }

  isAuthenticated(): boolean {
    return !!this.getAuthorizationToken();
  }

  authenticate(): Observable<any> {
    let userInfo: UserInfo = this.localStorage.get(this.localStorage.userInfo);
    return of(userInfo);
  }

  login(data: LoginRequest): Observable<LoginResponse> {
    return of({ token: 'fakeToken1' });
  }

  logout(): Observable<any> {
    return of({});
  }
}
