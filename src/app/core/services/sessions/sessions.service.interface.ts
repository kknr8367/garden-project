import { Observable } from 'rxjs';

export interface ISessionsService {
  getSessions(): Observable<any>;
}
