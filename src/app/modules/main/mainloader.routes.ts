import { Routes } from '@angular/router';
import { AuthGuard } from '@app/core/guards/auth.guard';
import { MainComponent } from './main.component';

export const appRoutes: Routes = [
  // {
  //     path: '',
  //     redirectTo: 'dashboard',
  //     pathMatch:'full'
  // },
  {
    path: '',
    component: MainComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'dashboard',
        data: {
          breadcrumb: [
            {
              label: 'Dashboard',
              url: '/dashboard',
            },
          ],
        },
        loadChildren: () =>
          import('@app/modules/dashboard/dashboard.module').then(
            (m) => m.DashboardModule
          ),
      },
      {
        path: 'sessions',
        data: {
          breadcrumb: [
            {
              label: 'Sessions',
              url: '/sessions',
            },
          ],
        },
        loadChildren: () =>
          import('@app/modules/sessions/sessions.module').then(
            (m) => m.SessionsModule
          ),
      },
    ],
  },
];
