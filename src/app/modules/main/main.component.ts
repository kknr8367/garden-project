import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { MediaChange, MediaObserver } from '@angular/flex-layout';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '@app/core/services/auth/auth.service';
import { DataService } from '@app/shared/services/data.service';
import { environment } from '@env/environment';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit, OnDestroy {
  @Input() isVisible: boolean = true;
  visibility: string = 'shown';

  sidenav: any = '';
  sideNavOpened: boolean = true;
  matDrawerOpened: boolean = false;
  matDrawerShow: boolean = true;
  sideNavMode: any = 'side';
  drawerMode: string = 'side';
  media$: Observable<MediaChange[]>;
  appName: string = environment.appName || 'MY APP';
  auth$!: Subscription;

  ngOnChanges() {
    this.visibility = this.isVisible ? 'shown' : 'hidden';
  }

  constructor(
    private media: MediaObserver,
    private auth: AuthService,
    private data: DataService
  ) {
    this.media$ = media.asObservable();
  }

  ngOnInit() {
    this.media$.subscribe(() => {
      this.toggleView();
    });

    this.auth$ = this.auth.authenticate().subscribe((user) => {
      if (user) {
        this.data.setUserInfo(user);
      }
    });
  }

  getRouteAnimation(outlet: { activatedRouteData: { animation: any } }) {
    return outlet.activatedRouteData.animation;
    //return outlet.isActivated ? outlet.activatedRoute : ''
  }

  toggleView() {
    if (this.media.isActive('gt-md')) {
      this.sideNavMode = 'side';
      this.sideNavOpened = false;
      this.matDrawerOpened = true;
      this.matDrawerShow = true;
    } else if (this.media.isActive('gt-xs')) {
      this.sideNavMode = 'side';
      this.sideNavOpened = false;
      this.matDrawerOpened = true;
      this.matDrawerShow = true;
    } else if (this.media.isActive('lt-sm')) {
      this.sideNavMode = 'over';
      this.sideNavOpened = false;
      this.matDrawerOpened = false;
      this.matDrawerShow = false;
    }
  }

  ngOnDestroy() {
    this.auth$.unsubscribe();
  }
}
