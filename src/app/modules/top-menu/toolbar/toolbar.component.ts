import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BreadcrumbService } from '@app/shared/services/breadcrumb.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent implements OnInit {
  @Input() sidenav: any;
  @Input() sidebar: any;
  @Input() drawer: any;
  @Input() matDrawerShow: any;

  constructor() {}

  ngOnInit() {}
}
