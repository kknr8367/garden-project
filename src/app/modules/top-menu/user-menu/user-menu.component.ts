import { Component, OnInit, ElementRef, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@app/core/services/auth/auth.service';
import { DataService } from '@app/shared/services/data.service';
import { LocalStorageService } from '@app/shared/services/local-storage.service';

@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss'],
})
export class UserMenuComponent implements OnInit {
  isOpen: boolean = false;
  currentUser: any | undefined;
  avatar: string | undefined;
  name: string = 'No name';

  constructor(
    private elementRef: ElementRef,
    private router: Router,
    private auth: AuthService,
    private dataService: DataService,
    private localStorage: LocalStorageService
  ) {}

  @HostListener('document:click', ['$event', '$event.target'])
  onClick(event: MouseEvent, targetElement: HTMLElement) {
    if (!targetElement) return;

    const clickedInside = this.elementRef.nativeElement.contains(targetElement);
    if (!clickedInside) {
      this.isOpen = false;
    }
  }

  ngOnInit() {
    this.dataService.getUserInfo().subscribe((data: any) => {
      this.currentUser = data || null;
    });
  }

  nagivateurl() {
    this.router.navigate(['/']);
    this.isOpen = false;
  }

  logout() {
    this.auth.logout().subscribe(() => {
      this.localStorage.removeAuth();
      this.router.navigateByUrl('/login');
    });
  }
}
