import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SessionsRoutingModule } from './sessions-routing.module';
import { ConversationComponent } from './conversation/conversation.component';
import { SharedModule } from '@app/shared/shared.module';
import { SessionsComponent } from './sessions/sessions.component';
import { AngularMaterialModule } from '@app/core/modules/angular-material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import {
  PerfectScrollbarConfigInterface,
  PerfectScrollbarModule,
  PERFECT_SCROLLBAR_CONFIG,
} from 'ngx-perfect-scrollbar';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
};

@NgModule({
  declarations: [SessionsComponent, ConversationComponent],
  imports: [
    CommonModule,
    AngularMaterialModule,
    SessionsRoutingModule,
    FormsModule,
    FlexLayoutModule,
    SharedModule,
    PerfectScrollbarModule,
  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
    },
  ],
})
export class SessionsModule {}
