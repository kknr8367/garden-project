import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConversationComponent } from './conversation/conversation.component';
import { SessionsComponent } from './sessions/sessions.component';

const routes: Routes = [
  {
    path: '',
    component: SessionsComponent,
  },
  {
    path: ':sessionId',
    data: {
      breadcrumb: [
        {
          label: 'Sessions',
          url: '/sessions',
        },
        {
          label: '{{ sessionId }}',
        },
      ],
    },
    component: ConversationComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SessionsRoutingModule {}
