import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BreadcrumbService } from '@app/shared/services/breadcrumb.service';

@Component({
  selector: 'app-conversation',
  templateUrl: './conversation.component.html',
  styleUrls: ['./conversation.component.scss'],
})
export class ConversationComponent implements OnInit {
  currentDate: Date = new Date();
  constructor(private activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {}
}
