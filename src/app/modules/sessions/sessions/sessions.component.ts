import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';

const ELEMENT_DATA: any[] = [
  {
    ucid: 11222132387328,
    startTime: moment().format(),
    handOverTime: 1.0079,
    lastQuery: 'Hey there',
    handledBy: 'Virtual Agent',
  },
  {
    ucid: 12221323873282,
    startTime: moment().format(),
    handOverTime: 4.0026,
    lastQuery: 'Hey there',
    handledBy: 'Virtual Agent',
  },
  {
    ucid: 12221323873283,
    startTime: moment().format(),
    handOverTime: 6.941,
    lastQuery: 'Hey there',
    handledBy: 'Virtual Agent',
  },
  {
    ucid: 12221323873284,
    startTime: moment().format(),
    handOverTime: 9.0122,
    lastQuery: 'Hey there',
    handledBy: 'Virtual Agent',
  },
  {
    ucid: 12221323873285,
    startTime: moment().format(),
    handOverTime: moment().add(1, 'd').format(),
    lastQuery: 'Hey there',
    handledBy: 'Virtual Agent',
  },
  {
    ucid: 12221323873286,
    startTime: moment().format(),
    handOverTime: moment().add(1, 'd').format(),
    lastQuery: 'Hey there',
    handledBy: 'Virtual Agent',
  },
  {
    ucid: 12221323873287,
    startTime: moment().format(),
    handOverTime: moment().add(1, 'd').format(),
    lastQuery: 'Hey there',
    handledBy: 'Virtual Agent',
  },
  {
    ucid: 12221323873288,
    startTime: moment().format(),
    handOverTime: moment().add(1, 'd').format(),
    lastQuery: 'Hey there',
    handledBy: 'Virtual Agent',
  },
  {
    ucid: 12221323873289,
    startTime: moment().format(),
    handOverTime: moment().add(1, 'd').format(),
    lastQuery: 'Hey there',
    handledBy: 'Virtual Agent',
  },
  {
    ucid: 122213238732810,
    startTime: moment().format(),
    handOverTime: moment().add(1, 'd').format(),
    lastQuery: 'Hey there',
    handledBy: 'Virtual Agent',
  },
];

@Component({
  selector: 'app-sessions',
  templateUrl: './sessions.component.html',
  styleUrls: ['./sessions.component.scss'],
})
export class SessionsComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = [
    'ucid',
    'startTime',
    'handOverTime',
    'lastQuery',
    'handledBy',
    'action',
  ];
  actions: Array<string> = ['join'];
  sentiments: Array<string> = [];

  isLoading: boolean = false;
  sessions: Array<any> = [];
  dataSource = new MatTableDataSource<any>(this.sessions);

  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.isLoading = true;
    setTimeout(() => {
      this.sessions = ELEMENT_DATA;
      this.dataSource = new MatTableDataSource<any>(this.sessions);
      this.dataSource.paginator = this.paginator;
      this.isLoading = false;
    }, 1000);
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(event: any) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onChangeFilter(event: any, type: string) {
    let params = {
      actions: this.actions,
      sentiments: this.sentiments,
    };
    // console.log('params', params);
  }

  action(element: any) {
    // console.log('element', element);
    this.router.navigate([`/sessions/${element.ucid}`]);
  }
}
