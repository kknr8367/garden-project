import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@app/core/services/auth/auth.service';
import { LoginRequest } from '@app/models/login';
import { LocalStorageService } from '@app/shared/services/local-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  state: LoginRequest = {
    emailId: '',
    password: '',
  };

  constructor(
    private auth: AuthService,
    private localStorage: LocalStorageService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  onSubmit() {
    this.auth.login(this.state).subscribe(({ token }) => {
      token = token || this.state.emailId;

      let data = {
        user: {
          username: this.state.emailId.split('@')[0],
          email: this.state.emailId,
        },
        token: token,
      };

      this.localStorage.setAuth(data);
      this.router.navigateByUrl('/sessions');
    });
  }

  oAuthLogin() {}
}
