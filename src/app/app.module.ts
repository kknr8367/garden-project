import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from './shared/shared.module';
import { SocketIoModule } from 'ngx-socket-io';
import { socketIoConfig } from './contants';
import { mockServiceProviders } from './mock';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SocketIoModule.forRoot(socketIoConfig),
    HttpClientModule,
    AppRoutingModule,
    SharedModule,
  ],
  providers: [...mockServiceProviders],
  bootstrap: [AppComponent],
})
export class AppModule {}
